(function(root, factory){
	if(typeof define === 'function' && define.amd){
		define(['pformat'], factory);
    }else if(typeof exports === 'object'){
        module.exports = factory(require('pformat'));
    } else {
		root.pformat = factory('pformat');
	}
})(this, function(pformat){
	'use strict';
	var conf = {
		lang : 'js',
		theme :'default'
	}
	var mark = new Array(false, false, false, false);//0:单行注释;1:多行注释;2:字符串;3:正则;
	var quote_mark = '';
	console.log(mark.indexOf(false));
	var keyword = {
		js : ['var', 'let', 'const', 'function', 'if', 'else', 'switch', 'case', 'continue', 'break', 'for', 'import', 'export', 'this', 'window', 'as', 'from', 'return', 'try', 'catch', 'class', 'in', 'of', 'new', 'while', 'async', 'await', 'throw', 'null', 'undefined']
	}
	var bool = ['false', 'true'];
	var sign = [' ', '\t', '(', ')', '{', '}', ';', ',', '.', ':', '=', '&', '|', '!', '[', ']'];
	var path = document.currentScript.src;
	path = path.substr(0, path.lastIndexOf('/')) + '/';
	function loadTheme(theme) {
		var css = path + 'theme/';
		if(arguments.length == 0) {
			css += 'default.css';
		} else {
			css += theme + '.css';
		}
		var link = document.createElement('link');
		link.rel = 'stylesheet';
		link.href = css;
		document.querySelector('head').appendChild(link);
	}
	function format(code) {
		var result = '<ol class="pformat-ol"><li class="pformat-li">';
		var length = code.length;
		var tmp = '';
		for(var i = 0; i < length; i++) {
			var character = code.substr(i, 1);
			if(/[\r\n]/.test(character)) {
				if(mark[0]) {
					result += `<span class="pformat-note">${tmp}</span></li><li class="pformat-li">`;
					mark[0] = false;
				} else if(mark[1]) {
					result += `<span class="pformat-note">${tmp}</span></li><li class="pformat-li">`;
					if(tmp.substr(tmp.length -2, 2) == '*/') mark[1] = false;
					console.log(tmp);
				} else if(mark[3]) {
					result += `<span class="pformat-reg">${tmp}</span>`;
					mark[3] = false;
				} else {
					result += `<span class="pformat-default">${tmp}</span></li><li class="pformat-li">`;
				}
				tmp = '';
			} else if(sign.includes(character)) {
				if(mark.includes(true)) {
					if(mark[3]) {
						var end = tmp.lastIndexOf('/');
						if(end > 3) {
							if(tmp.substr(end - 1, 1) != '\\') {
								console.log(tmp);
								result += `<span class="pformat-reg">${tmp}</span>${character}`;
								tmp = '';
								mark[3] = false;
							} else {
								tmp += character;
							}
						} else {
							tmp += character;
						}
					} else {
						tmp += character;
					}
				} else if(keyword.js.includes(tmp)) {
					result += `<span class="pformat-keyword">${tmp}</span>${character}`;
					tmp = '';
				} else if(/^(-?\d+)(\.\d+)?$/.test(tmp)) {
					result += `<span class="pformat-num">${tmp}</span>${character}`;
					tmp = '';
				} else if(bool.includes(tmp)) {
					result += `<span class="pformat-bool">${tmp}</span>${character}`;
					tmp = '';
				} else {
					result += `<span class="pformat-default">${tmp}${character}</span>`;
					tmp = '';
				}
			}else {
				if(character == '<') character = '&lt;';
				if(character == '>') character = '&gt;';
				tmp += character;
				if(tmp.length == 1) {
					if(tmp == '"' || tmp == "'" || tmp == '`') {
						mark[2] = true;
						quote_mark = tmp;
					} else if(character == '/' && code.substr(i + 1, 1) != '/' && code.substr(i + 1, 1) != '*') {
						mark[3] = true;
					}
				} else if(tmp.length == 2) {
					if(tmp == '//') {
						mark[0] = true;
					} else if(tmp == '/*') {
						mark[1] = true;
					} else if(character == quote_mark) {
						result += `<span class="pformat-str">${tmp}</span>`;
						mark[2] = false;
						quote_mark = '';
						tmp = '';
					}
				} else if(tmp.length > 2) {
					if(character == quote_mark) {
						result += `<span class="pformat-str">${tmp}</span>`;
						mark[2] = false;
						quote_mark = '';
						tmp = '';
					}
				}
			}
			if(character == '<' || character == '>') console.log(character);
		}
		result += '</li></ol>';
		return result;
	}
	function display(node, html) {
		node.innerHTML = html;
	}
	var target = document.getElementsByClassName('pformat');
	var length = target.length;
	for(var i = 0; i < length; i++) {
		var source = target[i].innerText;
		target[i].hasAttribute('theme') ? loadTheme(target[i].getAttribute('theme')) : loadTheme();
		var result = format(source);
		display(target[i], result);
	}
	return {
		version : 'v1.0.0_200509_Beta'
	}
});