# pformat

#### 介绍
轻量、简单的代码高亮组件/插件

#### 使用说明
```
<pre class="pformat">
代码块，'<','>'可直接输入，无需转义
</pre>
<script src="format路径"></script>
```
就这么简单

看看效果

![pformat效果图](https://images.gitee.com/uploads/images/2020/0509/174222_9da63b18_1470288.png "pformat效果图.png")

